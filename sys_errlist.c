#include <string.h>

const char *sys_errlist[132];
int sys_nerr = 132;
__attribute__((constructor)) static void f() {
  for (int i = 0; i < sys_nerr; i++)
    sys_errlist[i] = strdup(strerror(i));
}
